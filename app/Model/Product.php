<?php

namespace App\Model;

abstract class Product implements ProductInterface
{

    private int $id;
    private string $sku;
    private string $name;
    private float $price;
    private string $type;
    public string $attribute;


    public function __construct(
        int $id,
        string $sku,
        string $name,
        float $price,
        string $type,
        ?string $attribute = ''
    ) {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
        $this->attribute = $attribute;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAttribute(): string
    {
        return $this->attribute;
    }



}
