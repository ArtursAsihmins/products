<?php require_once __DIR__ . '/layouts/app.blade.php';

?>

<div class="container">
    <h5 class="mt-1">Product List</h5>
    <button type="submit" form="form-delete" class="btn btn-primary mt-1" id="apply">Apply
    </button>
    <hr/>
        <select class="custom-select mass-delete mt-1">
            <option value="">Choose...</option>
            <option value="mass-delete">Mass delete</option>
        </select>

<?php if(count($products) > 0): ?>
    <form action="" method="POST" id="form-delete">

    <div class="row justify-content-center">

            <?php foreach ($products as $product): ?>
            <div class="card m-3">
                <div class="card-body justify-content-center">
                    <p class="card-text"><?=$product['sku']?></p>
                    <p class="card-text"><?=$product['name']?></p>
                    <p class="card-text"><?=$product['price'] . ' $'?></p>
                    <p class="card-text">
                        <?php if ($product['type'] === 'dvd-disc'): ?>
                    <?= 'Size: ' . $product['attribute'] . ' MB' ?>
                    <?php elseif ($product['type'] === 'book'): ?>
                    <?= 'Weight: ' . $product['attribute'] . ' KG' ?>
                    <?php else: ?>
                    <?= 'Dimension: ' . $product['attribute'] ?>

                        <?php endif ?>
                    </p>
                   <input type='checkbox' name='delete[]'
                                 value='<?= $product['id'] ?>' class="selected" >

                </div>
            </div>

            <?php endforeach; ?>
    </div>
    </form>
    <?php else:?>
    <p>There currently are no products!!!</p>
    <?php endif;?>

</div>

