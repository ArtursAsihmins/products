<?php require_once __DIR__ . '/layouts/app.blade.php' ?>


<div class="container">
    <h5 class="mt-1">Product Add</h5>
    <button type="submit" class="btn btn-primary mt-1"  id="save"
            form="product-form">Save
    </button>

    <hr/>
    <div class="row justify-content-left">
        <div class="col-3">
            <form action="" method="POST" id="product-form">
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control" name="sku" id="sku" required>
                    <span id="error_sku"></span>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" step="0.01" name="price" id="price" required>
                </div>
                <div class="form-group">
                    <label for="type-switcher">Type Switcher</label>
                    <select name="type-switcher" id="type-switcher" class="custom-select" required>
                        <option value="">Type Switcher</option>
                        <option value="dvd-disc">DVD-disc</option>
                        <option value="book">Book</option>
                        <option value="furniture" name="furniture">Furniture</option>
                    </select>
                </div>
                <div class="form-group selected " id="dvd-disc" >
                    <label for="size">Size</label>
                    <input type="number" name="size" id="size" class="form-control">
                    <span>Please provide size in MB format</span>
                </div>
                <div class="form-group selected" id="book" >
                    <label for="weight">Weight</label>
                    <input type="number" name="weight" id="weight" class="form-control">
                    <span>Please provide weight in KG format</span>
                </div>
                <div class="form-group selected" id="furniture" >
                    <label for="height">Height</label>
                    <input type="number" name="height" id="height" class="form-control">
                    <label for="width">Width</label>
                    <input type="number" name="width" id="width" class="form-control">
                    <label for="length">Length</label>
                    <input type="number" name="length" id="length" class="form-control">
                    <span>Please provide dimensions in HxWxL format</span>
                </div>

            </form>


        </div>
    </div>
</div>
