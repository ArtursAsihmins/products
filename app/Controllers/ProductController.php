<?php

namespace App\Controllers;

use App\Model\Book;
use App\Model\DVDdisc;
use App\Model\Furniture;


class ProductController
{

    public function index()
    {
        require_once __DIR__ . '/../Database/config.php';

        $sql = $conn->prepare("SELECT * FROM products");
        $sql->execute();
        $products = $sql->fetchAll(\PDO::FETCH_ASSOC);


        return require_once __DIR__ . '/../Views/index.blade.php';
    }

    public function create()
    {
        return require_once __DIR__ . '/../Views/create.blade.php';
    }

    public function store()
    {
        require_once __DIR__ . '/../Database/config.php';
        require_once __DIR__ . '/../Views/create.blade.php';
        $sku = $_REQUEST['sku'];
        $name = $_REQUEST['name'];
        $price = $_REQUEST['price'];
        $type = $_REQUEST['type-switcher'];

        if ($type === 'dvd-disc') {
            $product = new DVDdisc(0, $sku, $name, $price, $type);
            $product->attribute();
        } elseif ($type === 'book') {
            $product = new Book(0, $sku, $name, $price, $type);
            $product->attribute();
        } else {

            $product = new Furniture(0, $sku, $name, $price, $type);
            $product->attribute();

        }

        serialize($product);

        $sql = "INSERT INTO products (sku, name, price, type, attribute) VALUES (
?,?,?,?,?)";

        $conn->prepare($sql)->execute([
            $product->getSku(),
            $product->getName(),
            $product->getPrice(),
            $product->getType(),
            $product->getAttribute()
        ]);
    }


    public function delete()
    {
        require_once __DIR__ . '/../Database/config.php';

        foreach ($_REQUEST['delete'] as $key => $value) {
            $sql = $conn->prepare("DELETE FROM products WHERE id=$value");
            $sql->execute();
        }
        header("Location: /products");
    }

    public function checkSku()
    {
        require_once __DIR__ . '/../Database/config.php';


        if ($_REQUEST['sku']) {
            $sku = $_REQUEST['sku'];
            $sql = $conn->prepare("SELECT sku FROM products WHERE sku=$sku");
            $sql->execute();


            if ($sql->rowCount() > 0) {
                echo 'not_unique';
            } else {
                echo 'unique';
            }
        }
    }

}
