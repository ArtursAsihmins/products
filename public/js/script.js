$(document).ready(function () {
    $('#sku').blur(function () {
        const sku = $(this).val();
            $.ajax({
                url: "/check",
                method: "POST",
                data: {sku: sku},
                success: function (result) {
                    if (result === 'unique') {
                        $('#error_sku').html('<label class="text-success">SKU available</label>');
                        $('#sku').removeClass('has-error');
                        $('#save').attr('disabled', false);
                    } else {
                        $('#error_sku').html('<label class="text-danger">SKU not available</label>');
                        $('#sku').addClass('has-error');
                        $('#save').attr('disabled', 'disabled');
                    }
                }
            })
    });
})


$(function () {
    $('.mass-delete').change(function () {
        if($(this).find("option:selected").attr("value") === "mass-delete") {
            $('.selected').show();
        } else {
            $('.selected').hide();
        }
    });
});


$(function () {
    $('#type-switcher').change(function () {
        $('.selected').hide();
        $('#' + $(this).val()).show();
    });
});
